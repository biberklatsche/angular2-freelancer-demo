package de.softwareforen;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.softwareforen.freelancer.persistance.entity.Category;
import de.softwareforen.freelancer.persistance.repository.CategoryRepository;
import de.softwareforen.freelancer.presentation.controller.rest.FreelancerTransformer;
import de.softwareforen.freelancer.presentation.dto.FreelancerDTO;
import de.softwareforen.freelancer.presentation.dto.CategoryDTO;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.data.rest.RepositoryRestProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentation;
import org.springframework.restdocs.payload.PayloadDocumentation;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("rawtypes")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = FreelancerApplication.class)
@WebAppConfiguration
@ActiveProfiles({"documentation"})
@EnableConfigurationProperties(RepositoryRestProperties.class)
public class RestEndpointDocumentation {

    @Rule
    public RestDocumentation restDocumentation = new RestDocumentation("target/generated-snippets");

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Autowired
    @Qualifier("objectMapperWithoutNull")
    private ObjectMapper objectMapper;

    @Autowired
    private CategoryRepository categoryRepository;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).apply(documentationConfiguration(this.restDocumentation)).build();

    }

    @Test
    public void restAngularFreelancers() throws Exception {
        this.mockMvc.perform(get("/rest/angular/freelancers").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andDo(document("rest/angular/freelancers",
                PayloadDocumentation.responseFields(PayloadDocumentation.fieldWithPath("freelancers").description("Liste mit allen vorhandenen Freelancern"))));
    }

    @Test
    public void restAngularCategories() throws Exception {
        this.mockMvc.perform(get("/rest/angular/categories").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andDo(
                document("rest/angular/categories", PayloadDocumentation
                        .responseFields(PayloadDocumentation.fieldWithPath("categories").description("Liste mit allen vorhandenen Kategorien"),
                                PayloadDocumentation.fieldWithPath("categories[].name").description("Name der Kategorie"),
                                PayloadDocumentation.fieldWithPath("categories[].id").description("Id der Kategorie"))));
    }

    @Test
    public void restAngularFreelancerSaveAndDelete() throws Exception {
        String contentResult = this.mockMvc.perform(post("/rest/angular/freelancer/save").content(createFreelancerDTOContent()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andDo(document("rest/angular/freelancer/save", PayloadDocumentation
                        .requestFields(PayloadDocumentation.fieldWithPath("category").description("Kategorie des Freelancers"),
                                PayloadDocumentation.fieldWithPath("firstname").description("Vorname des Freelancers"),
                                PayloadDocumentation.fieldWithPath("lastname").description("Nachname des Freelancers")))).andReturn().getResponse().getContentAsString();

        FreelancerDTO freelancerDTO = objectMapper.readValue(contentResult, FreelancerDTO.class);

        this.mockMvc.perform(delete("/rest/angular/freelancer/delete/{id}", freelancerDTO.getId())).andExpect(status().isOk())
                .andDo(document("rest/angular/freelancer/delete", pathParameters(parameterWithName("id").description("Id des Freelancers welches gelöscht werden soll"))));
    }

    private String createFreelancerDTOContent() throws JsonProcessingException {
        FreelancerDTO freelancerDTO = new FreelancerDTO();
        freelancerDTO.setCategory(createCategoryDTO());
        freelancerDTO.setFirstname("Lars");
        freelancerDTO.setLastname("Wolfram");
        return objectMapper.writeValueAsString(freelancerDTO);
    }

    private CategoryDTO createCategoryDTO() {
        Category category = categoryRepository.findOne(1L);
        return FreelancerTransformer.transform(category);
    }
}
