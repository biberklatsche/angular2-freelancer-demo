package de.softwareforen.freelancer.presentation.controller;

import de.softwareforen.freelancer.exception.VAException;
import de.softwareforen.freelancer.helper.DTOCreator;
import de.softwareforen.freelancer.helper.EntityCreator;
import de.softwareforen.freelancer.persistance.entity.Category;
import de.softwareforen.freelancer.persistance.entity.Freelancer;
import de.softwareforen.freelancer.presentation.controller.rest.FreelancerController;
import de.softwareforen.freelancer.presentation.dto.FreelancerDTO;
import de.softwareforen.freelancer.presentation.dto.CategoriesDTO;
import de.softwareforen.freelancer.presentation.dto.FreelancersDTO;
import de.softwareforen.freelancer.service.FreelancerService;
import de.softwareforen.freelancer.service.CategoryService;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.stream.Collectors;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.*;

/**
 * Created by lwo on 30.06.2016.
 */
public class FreelancerControllerTest {

    @Test
    public void delete_inexistingFreelancer() {

        FreelancerService freelancerServiceMock = Mockito.mock(FreelancerService.class);
        when(freelancerServiceMock.findById(-1)).thenReturn(null);

        FreelancerController controller = createTestObject(freelancerServiceMock);
        controller.delete(-1);

        verify(freelancerServiceMock, times(0)).delete(anyObject());
    }

    @Test
    public void delete_existingFreelancer() {

        Freelancer freelancerToDelete = new Freelancer();
        freelancerToDelete.setId(12371l);
        FreelancerService freelancerServiceMock = Mockito.mock(FreelancerService.class);
        when(freelancerServiceMock.findById(freelancerToDelete.getId())).thenReturn(freelancerToDelete);

        FreelancerController controller = createTestObject(freelancerServiceMock);
        controller.delete(freelancerToDelete.getId());

        verify(freelancerServiceMock, times(1)).delete(freelancerToDelete);
    }

    @Test
    public void save() throws VAException {

        long expectedFreelancerId = 3248;

        FreelancerDTO freelancerDTO = DTOCreator.createFreelancerDTO();
        Category category = EntityCreator.createCategory();

        CategoryService categoryServiceMock = mockCategoryServiceFindByName(freelancerDTO, category);

        FreelancerService freelancerServiceMock = mockFreelancerService(expectedFreelancerId);

        FreelancerController controller = createTestObject(freelancerServiceMock, categoryServiceMock);
        FreelancerDTO savedFreelancerDTO = controller.save(freelancerDTO);

        assertThat(savedFreelancerDTO.getId(), is(expectedFreelancerId));
    }

    @Test
    public void listCategories() {

        Category category1 = EntityCreator.createCategory();
        Category category2 = EntityCreator.createCategory();

        CategoryService categoryServiceMock = mockCategoryServiceFindAll(category1, category2);

        FreelancerController controller = createTestObject(categoryServiceMock);
        CategoriesDTO types = controller.listCategories();

        assertThat(types.getCategories().size(), is(2));
    }

    private CategoryService mockCategoryServiceFindAll(Category... categories) {
        CategoryService categoryServiceMock = Mockito.mock(CategoryService.class);
        when(categoryServiceMock.findAll()).thenReturn(Arrays.stream(categories).collect(Collectors.toList()));
        return categoryServiceMock;
    }

    @Test
    public void listFreelancers() {

        Freelancer freelancer1 = EntityCreator.createFreelancer();
        Freelancer freelancer2 = EntityCreator.createFreelancer();

        FreelancerService freelancerServiceMock = mockFreelancerServiceFindAll(freelancer1, freelancer2);

        FreelancerController controller = createTestObject(freelancerServiceMock);
        FreelancersDTO freelancers = controller.listFreelancers();

        assertThat(freelancers.getFreelancers().size(), is(2));
    }

    private FreelancerService mockFreelancerServiceFindAll(Freelancer... freelancers) {
        FreelancerService freelancerServiceMock = Mockito.mock(FreelancerService.class);
        when(freelancerServiceMock.findAll()).thenReturn(Arrays.stream(freelancers).collect(Collectors.toList()));
        return freelancerServiceMock;
    }

    private FreelancerService mockFreelancerService(long expectedFreelancerId) {
        FreelancerService freelancerServiceMock = Mockito.mock(FreelancerService.class);
        doAnswer(invocationOnMock -> {
            Object[] args = invocationOnMock.getArguments();
            Freelancer freelancer = (Freelancer) args[0];
            freelancer.setId(expectedFreelancerId);
            return null;
        }).when(freelancerServiceMock).save(any(Freelancer.class));
        return freelancerServiceMock;
    }

    private CategoryService mockCategoryServiceFindByName(FreelancerDTO freelancerDTO, Category category) {
        CategoryService categoryServiceMock = Mockito.mock(CategoryService.class);
        when(categoryServiceMock.findByName(freelancerDTO.getCategory().getName())).thenReturn(category);
        return categoryServiceMock;
    }


    private FreelancerController createTestObject(FreelancerService freelancerServiceMock) {
        return this.createTestObject(freelancerServiceMock, Mockito.mock(CategoryService.class));
    }

    private FreelancerController createTestObject(CategoryService categoryServiceMock) {
        return this.createTestObject(Mockito.mock(FreelancerService.class), categoryServiceMock);
    }

    private FreelancerController createTestObject(FreelancerService freelancerServiceMock, CategoryService categoryServiceMock) {
        FreelancerController controller = new FreelancerController(freelancerServiceMock, categoryServiceMock);
        return controller;
    }
}
