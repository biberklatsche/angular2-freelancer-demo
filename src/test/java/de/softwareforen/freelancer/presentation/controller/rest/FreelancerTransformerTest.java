package de.softwareforen.freelancer.presentation.controller.rest;

import de.softwareforen.freelancer.helper.DTOCreator;
import de.softwareforen.freelancer.helper.EntityCreator;
import de.softwareforen.freelancer.persistance.entity.Category;
import de.softwareforen.freelancer.persistance.entity.Freelancer;
import de.softwareforen.freelancer.presentation.dto.FreelancerDTO;
import de.softwareforen.freelancer.presentation.dto.CategoryDTO;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by lwo on 29.06.2016.
 */
public class FreelancerTransformerTest {

    @Test
    public void transformFreelancerDTO_null() {
        FreelancerDTO dto = null;
        Freelancer freelancer = FreelancerTransformer.transform(dto);
        assertThat(freelancer, is(nullValue()));
    }

    @Test
    public void transformFreelancerDTO_emptyDTO() {
        FreelancerDTO dto = new FreelancerDTO();
        Freelancer freelancer = FreelancerTransformer.transform(dto);
        assertThat(freelancer, isEqualTo(dto));
    }

    @Test
    public void transformFreelancerDTO() {
        FreelancerDTO dto = DTOCreator.createFreelancerDTO();
        Freelancer freelancer = FreelancerTransformer.transform(dto);
        assertThat(freelancer, isEqualTo(dto));
    }

    @Test
    public void transformFreelancer_null() {
        Freelancer freelancer = null;
        FreelancerDTO dto = FreelancerTransformer.transform(freelancer);
        assertThat(dto, is(nullValue()));
    }

    @Test
    public void transformFreelancer_empty() {
        Freelancer freelancer = new Freelancer();
        FreelancerDTO dto = FreelancerTransformer.transform(freelancer);
        assertThat(dto, isEqualTo(freelancer));
    }

    @Test
    public void transformFreelancer() {
        Freelancer freelancer = EntityCreator.createFreelancer();
        FreelancerDTO dto = FreelancerTransformer.transform(freelancer);
        assertThat(dto, isEqualTo(freelancer));
    }

    @Test
    public void transformCategoryDTO_null() {
        CategoryDTO dto = null;
        Category freelancer = FreelancerTransformer.transform(dto);
        assertThat(freelancer, is(nullValue()));
    }

    @Test
    public void transformCategoryDTO_emptyDTO() {
        CategoryDTO dto = new CategoryDTO();
        Category freelancer = FreelancerTransformer.transform(dto);
        assertThat(freelancer, isEqualTo(dto));
    }

    @Test
    public void transformCategoryDTO() {
        CategoryDTO dto = DTOCreator.createCategoryDTO();
        Category category = FreelancerTransformer.transform(dto);
        assertThat(category, isEqualTo(dto));
    }

    @Test
    public void transformCategory_null() {
        CategoryDTO dto = null;
        Category category = FreelancerTransformer.transform(dto);
        assertThat(category, is(nullValue()));
    }

    @Test
    public void transformCategory_empty() {
        CategoryDTO dto = new CategoryDTO();
        Category category = FreelancerTransformer.transform(dto);
        assertThat(category, isEqualTo(dto));
    }

    @Test
    public void transformCategory() {
        Category category = EntityCreator.createCategory();
        CategoryDTO categoryDTO = FreelancerTransformer.transform(category);
        assertThat(categoryDTO, isEqualTo(category));
    }

    private Matcher<CategoryDTO> isEqualTo(Category category) {
        return new TypeSafeDiagnosingMatcher<CategoryDTO>() {

            @Override
            public void describeTo(Description description) {
                description.appendText("Fields in category and categoryDto are equal.");
            }

            @Override
            protected boolean matchesSafely(CategoryDTO dto, Description description) {
                return Comparator.equals(category, dto, description);
            }
        };
    }

    private Matcher<Category> isEqualTo(CategoryDTO dto) {
        return new TypeSafeDiagnosingMatcher<Category>() {

            @Override
            public void describeTo(Description description) {
                description.appendText("Fields in category and categoryDto are equal.");
            }

            @Override
            protected boolean matchesSafely(Category category, Description description) {
                return Comparator.equals(category, dto, description);
            }
        };
    }

    private Matcher<Freelancer> isEqualTo(FreelancerDTO dto) {
        return new TypeSafeDiagnosingMatcher<Freelancer>() {

            @Override
            public void describeTo(Description description) {
                description.appendText("Fields in freelancer and freelancerDto are equal.");
            }

            @Override
            protected boolean matchesSafely(Freelancer freelancer, Description description) {
                return Comparator.equals(freelancer, dto, description);
            }
        };
    }

    private Matcher<FreelancerDTO> isEqualTo(Freelancer freelancer) {
        return new TypeSafeDiagnosingMatcher<FreelancerDTO>() {

            @Override
            public void describeTo(Description description) {
                description.appendText("Fields in freelancer and freelancerDto are equal.");
            }

            @Override
            protected boolean matchesSafely(FreelancerDTO dto, Description description) {
                return Comparator.equals(freelancer, dto, description);
            }
        };
    }

    private static class Comparator {
        public static boolean equals(Category category, CategoryDTO dto, Description description) {
            boolean matchAllValues = true;
            if (!areBothNull(category.getName(), dto.getName()) && !category.getName().equals(dto.getName())) {
                description.appendText("name is different ").appendValue(dto.getName());
                matchAllValues = false;
            }
            if (category.getId() != dto.getId()) {
                description.appendText("id is different. expected: ");
                matchAllValues = false;
            }

            return matchAllValues;
        }

        public static boolean equals(Freelancer freelancer, FreelancerDTO dto, Description description) {
            boolean matchAllValues = true;
            if (!areBothNull(freelancer.getFirstname(), dto.getFirstname())
                    && !freelancer.getFirstname().equals(dto.getFirstname())) {
                description.appendText("firstname is different");
                matchAllValues = false;
            }
            if (!areBothNull(freelancer.getLastname(), dto.getLastname())
                    && !freelancer.getFirstname().equals(dto.getFirstname())) {
                description.appendText("lastname is different");
                matchAllValues = false;
            }
            if (!areBothNull(freelancer.getCategory(), dto.getCategory())
                    && !freelancer.getCategory().getName().equals(dto.getCategory().getName())) {
                description.appendText("category is different");
                matchAllValues = false;
            }
            if (freelancer.getId() != dto.getId()) {
                description.appendText("id is different. expected: ");
                matchAllValues = false;
            }
            if (freelancer.getPricePerDay() != dto.getPricePerDay()) {
                description.appendText("pricePerDay is different. expected: ");
                matchAllValues = false;
            }
            return matchAllValues;
        }

        private static boolean areBothNull(Object a, Object b) {
            return a == null && b == null;
        }
    }
}
