package de.softwareforen.freelancer.helper;

import de.softwareforen.freelancer.persistance.entity.Category;
import de.softwareforen.freelancer.persistance.entity.Freelancer;

/**
 * Created by lwo on 30.06.2016.
 */
public class EntityCreator {
    public static Category createCategory() {
        Category category = new Category();
        category.setId(124l);
        category.setName("Category");
        return category;
    }

    public static Freelancer createFreelancer() {
        Freelancer freelancer = new Freelancer();
        freelancer.setId(1l);
        freelancer.setFirstname("Lars");
        freelancer.setLastname("Wolfram");
        freelancer.setCategory(EntityCreator.createCategory());
        freelancer.setPricePerDay(10000);
        return freelancer;
    }
}
