package de.softwareforen.freelancer.helper;

import de.softwareforen.freelancer.presentation.dto.FreelancerDTO;
import de.softwareforen.freelancer.presentation.dto.CategoryDTO;

/**
 * Created by lwo on 30.06.2016.
 */
public class DTOCreator {


    public static CategoryDTO createCategoryDTO() {
        CategoryDTO category = new CategoryDTO();
        category.setId(1l);
        category.setName("Category");
        return category;
    }

    public static FreelancerDTO createFreelancerDTO() {
        FreelancerDTO freelancerDTO = new FreelancerDTO();
        freelancerDTO.setFirstname("Lars");
        freelancerDTO.setLastname("Wolfram");
        freelancerDTO.setId(1231L);
        freelancerDTO.setCategory(createCategoryDTO());
        freelancerDTO.setPricePerDay(10000);
        return freelancerDTO;
    }


}
