package de.softwareforen.freelancer.presentation.controller.rest;

import de.softwareforen.freelancer.exception.VAException;
import de.softwareforen.freelancer.persistance.entity.Category;
import de.softwareforen.freelancer.persistance.entity.Freelancer;
import de.softwareforen.freelancer.presentation.dto.CategoriesDTO;
import de.softwareforen.freelancer.presentation.dto.CategoryDTO;
import de.softwareforen.freelancer.presentation.dto.FreelancerDTO;
import de.softwareforen.freelancer.presentation.dto.FreelancersDTO;
import de.softwareforen.freelancer.service.CategoryService;
import de.softwareforen.freelancer.service.FreelancerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class FreelancerController {

    private static final Logger logger = LoggerFactory.getLogger(FreelancerController.class);

    private final FreelancerService freelancerService;

    private final CategoryService categoryService;

    @Autowired
    public FreelancerController(FreelancerService freelancerService, CategoryService categoryService) {
        this.freelancerService = freelancerService;
        this.categoryService = categoryService;
    }


    @RequestMapping(value="/rest/angular/freelancer/delete/{id}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public void delete(@PathVariable long id) {
        Freelancer freelancer = freelancerService.findById(id);
        if (freelancer != null) {
            freelancerService.delete(freelancer);
        }
    }

    @RequestMapping("/rest/angular/categories")
    @ResponseBody
    public CategoriesDTO listCategories() {
        Iterable<Category> categories = categoryService.findAll();
        List<CategoryDTO> categoryDTOs = new ArrayList<>();
        for (Category category : categories) {
            categoryDTOs.add(FreelancerTransformer.transform(category));
        }
        CategoriesDTO types = new CategoriesDTO();
        types.setCategories(categoryDTOs);
        return types;
    }

    @RequestMapping("/rest/angular/freelancers")
    @ResponseBody
    public FreelancersDTO listFreelancers() {
        Iterable<Freelancer> freelancers = freelancerService.findAll();
        List<FreelancerDTO> freelancerDTOs = new ArrayList<>();
        for (Freelancer freelancer : freelancers) {
            freelancerDTOs.add(FreelancerTransformer.transform(freelancer));
        }
        FreelancersDTO freelancersDTO = new FreelancersDTO();
        freelancersDTO.setFreelancers(freelancerDTOs);
        return freelancersDTO;
    }

    @RequestMapping("/rest/angular/freelancer/{id}")
    @ResponseBody
    public FreelancerDTO getFreelancer(@PathVariable long id) {
        Freelancer freelancer = freelancerService.findById(id);
        FreelancerDTO freelancerDTO = FreelancerTransformer.transform(freelancer);
        return freelancerDTO;
    }

    @RequestMapping(value = "/rest/angular/freelancer/save", method = RequestMethod.POST,
            headers =
                    {
                            "Content-type=application/json"
                    })
    @ResponseBody
    public FreelancerDTO save(@RequestBody FreelancerDTO freelancerDTO) throws VAException {
        Freelancer freelancer = FreelancerTransformer.transform(freelancerDTO);
        Category category = categoryService.findById(freelancerDTO.getCategory().getId());
        freelancer.setCategory(category);
        freelancerService.save(freelancer);
        return FreelancerTransformer.transform(freelancer);
    }
}
