package de.softwareforen.freelancer.presentation.controller.rest;

import de.softwareforen.freelancer.persistance.entity.Category;
import de.softwareforen.freelancer.persistance.entity.Freelancer;
import de.softwareforen.freelancer.presentation.dto.FreelancerDTO;
import de.softwareforen.freelancer.presentation.dto.CategoryDTO;

/**
 * Created by Lars on 21.11.2015.
 */
public class FreelancerTransformer {
    public static Freelancer transform(FreelancerDTO dto) {
        if (dto == null) {
            return null;
        }
        Freelancer freelancer = new Freelancer();
        freelancer.setFirstname(dto.getFirstname());
        freelancer.setLastname(dto.getLastname());
        freelancer.setId(dto.getId());
        freelancer.setCategory(transform(dto.getCategory()));
        freelancer.setPricePerDay(dto.getPricePerDay());
        return freelancer;
    }

    public static Category transform(CategoryDTO categoryDTO) {
        if (categoryDTO == null) {
            return null;
        }
        Category category = new Category();
        category.setName(categoryDTO.getName());
        category.setId(categoryDTO.getId());
        return category;
    }

    public static CategoryDTO transform(Category category) {
        if (category == null) {
            return null;
        }
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setName(category.getName());
        categoryDTO.setId(category.getId());
        return categoryDTO;
    }

    public static FreelancerDTO transform(Freelancer freelancer) {
        if (freelancer == null) {
            return null;
        }
        FreelancerDTO dto = new FreelancerDTO();
        dto.setId(freelancer.getId());
        dto.setCategory(transform(freelancer.getCategory()));
        dto.setFirstname(freelancer.getFirstname());
        dto.setLastname(freelancer.getLastname());
        dto.setPricePerDay(freelancer.getPricePerDay());
        return dto;
    }
}
