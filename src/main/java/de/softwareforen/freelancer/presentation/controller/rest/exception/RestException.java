package de.softwareforen.freelancer.presentation.controller.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Lars on 22.11.2015.
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR) // 500
public class RestException extends RuntimeException {
    public RestException(String message) {
        super(message);
    }
}
