package de.softwareforen.freelancer.presentation.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class WebController {

    @RequestMapping("/")
    public String getIndex() {
        return "/index.html";
    }

    @RequestMapping("favicon.ico")
    @ResponseBody
    void favicon() { /* empty method in conformance with Spring Standards */}
}
