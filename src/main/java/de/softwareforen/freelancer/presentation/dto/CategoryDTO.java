package de.softwareforen.freelancer.presentation.dto;

/**
 * Created by larswolfram on 25.11.15.
 */
public class CategoryDTO {
    private String name;
    private Long id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
