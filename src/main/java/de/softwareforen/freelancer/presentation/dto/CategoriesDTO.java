package de.softwareforen.freelancer.presentation.dto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by larswolfram on 25.11.15.
 */
public class CategoriesDTO {

    private List<CategoryDTO> categories;

    public List<CategoryDTO> getCategories() {
        if (categories == null) {
            return Collections.emptyList();
        } else {
            return Collections.unmodifiableList(categories);
        }
    }

    public void setCategories(List<CategoryDTO> categoryDTOs) {
        if (categoryDTOs != null) {
            this.categories = new ArrayList(categoryDTOs);
        }
    }
}
