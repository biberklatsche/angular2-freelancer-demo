package de.softwareforen.freelancer.presentation.dto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Lars on 16.11.2015.
 */
public class FreelancersDTO {
    private List<FreelancerDTO> freelancers = new LinkedList<>();

    public List<FreelancerDTO> getFreelancers() {
        if (freelancers == null) {
            return Collections.emptyList();
        } else {
            return Collections.unmodifiableList(freelancers);
        }
    }

    public void setFreelancers(List<FreelancerDTO> freelancerDTOs) {
        if (freelancerDTOs != null) {
            this.freelancers = new ArrayList(freelancerDTOs);
        }
    }
}
