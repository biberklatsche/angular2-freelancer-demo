package de.softwareforen.freelancer.service;

import de.softwareforen.freelancer.persistance.entity.Freelancer;
import de.softwareforen.freelancer.persistance.repository.FreelancerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FreelancerService {

    private final FreelancerRepository freelancerRepository;

    @Autowired
    public FreelancerService(FreelancerRepository freelancerRepository) {
        this.freelancerRepository = freelancerRepository;
    }

    public Iterable<Freelancer> findAll() {
        return freelancerRepository.findAll();
    }

    public Freelancer findById(long id) {
        return freelancerRepository.findOne(id);
    }

    public void save(Freelancer freelancer) {
        freelancerRepository.save(freelancer);
    }

    public void delete(Freelancer freelancer) {
        freelancerRepository.delete(freelancer);
    }
}
