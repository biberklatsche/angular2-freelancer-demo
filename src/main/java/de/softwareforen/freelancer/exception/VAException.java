package de.softwareforen.freelancer.exception;

public class VAException extends Exception {

    private static final long serialVersionUID = -2185924300134068374L;

    public VAException(String message) {
        super(message);
    }

    public VAException(String message, Exception ex) {
        super(message, ex);
    }
}
