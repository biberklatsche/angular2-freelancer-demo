package de.softwareforen.freelancer.exception;

public class ParseException extends VAException {

    private static final long serialVersionUID = 8148594368928482150L;

    public ParseException(String message) {
        super(message);
    }

    public ParseException(String message, Exception ex) {
        super(message, ex);
    }

}
