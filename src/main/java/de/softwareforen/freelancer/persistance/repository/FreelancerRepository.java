package de.softwareforen.freelancer.persistance.repository;

import de.softwareforen.freelancer.persistance.entity.Freelancer;
import de.softwareforen.freelancer.persistance.entity.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FreelancerRepository extends CrudRepository<Freelancer, Long> {

    List<Freelancer> findByLastname(String lastname);

    List<Freelancer> findByFirstname(String firstname);

    List<Freelancer> findByCategory(Category type);
}
