package de.softwareforen.freelancer.persistance.repository;

import de.softwareforen.freelancer.persistance.entity.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category, Long> {
    Category findByName(String name);
}
