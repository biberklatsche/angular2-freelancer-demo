import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Freelancer} from '../model/freelancer';
import {Category} from '../model/category';
import {Injectable} from '@angular/core';

@Injectable()
export class BackendService {
    private categoriesUrl = 'http://localhost:8080/rest/angular/categories';
    private freelancersUrl = 'http://localhost:8080/rest/angular/freelancers';
    private freelancerDeleteUrl = 'http://localhost:8080/rest/angular/freelancer/delete/';
    private freelancerUrl = 'http://localhost:8080/rest/angular/freelancer/';
    private freelancerSaveUrl = 'http://localhost:8080/rest/angular/freelancer/save';
    private freelancers: Array<Freelancer>;
    private categories: Array<Category>;

    constructor(private http: Http) { }

    getFreelancers() {
        return this.http.get(this.freelancersUrl)
            .map(res => res.json().freelancers)
            .map(rawFreelancers => rawFreelancers.map(Freelancer.create));
    }

    deleteFreelancer(id: number) {
        let url = this.freelancerDeleteUrl + id;
        return this.http.get(url);
    }

    getFreelancer(id: number){
        let url = this.freelancerUrl + id;
        return this.http.get(url)
            .map(res => res.json().freelancer)
            .map(rawFreelancer => Freelancer.create);
    }

    saveFreelancer(freelancer: Freelancer) {
        let url = this.freelancerSaveUrl;
        let body = JSON.stringify(freelancer);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(url, body, options)
            .map(res => res.json().freelancer);
    }

    getCategories(): Observable<Array<Category>> {
        return this.http.get(this.categoriesUrl)
            .map(res => res.json().categories)
            .map(rawCategories => rawCategories.map(Category.create));
    }

    private handleError(error) {
        console.log(error);
    }
}