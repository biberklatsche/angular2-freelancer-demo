import {Category} from './category';

export class Freelancer{
    private static centPerEuro = 100;
    id: number;
    firstname : String;
    lastname : String;
    category : Category;
    price : Number;
    
    
    constructor(id: number, firstname: String, lastname: String, price:Number ,category: Category){
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.category = category;
        this.price = price;
    }

    static new(){
        return new Freelancer(null, "" , "", 0, null);
    }

    static create(data){
        return new Freelancer(data.id, data.firstname, data.lastname, data.pricePerDay / Freelancer.centPerEuro , new Category(data.category.id, data.category.name));
    }

    clone(){
        return new Freelancer(this.id, this.firstname, this.lastname, this.price, this.category);
    }

    copyValues(other : Freelancer ){
        this.id = other.id;
        this.firstname = other.firstname;
        this.lastname = other.lastname;
        this.price = other.price;
        this.category = other.category;
    }
}