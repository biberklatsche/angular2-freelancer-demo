export class Category{
    id: number;
    name : String;
    
    constructor(id: number, name: String){
        this.id = id;
        this.name = name;
    }

    static create(data){
        return new Category(data.id, data.name);
    }
}