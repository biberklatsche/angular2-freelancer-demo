import {Component} from '@angular/core';
import {Category} from '../../model/category';
import {BackendService} from '../../backend/backend.service';

@Component({
    selector: 'category-list',
    moduleId: module.id,
    templateUrl: 'categoryList.component.html',
    styleUrls: ['categoryList.component.css']
})
export class CategoryListComponent {
    private categoryList: Array<Category>;
    private backendService: BackendService;

    constructor(backendService: BackendService) {
        this.backendService = backendService;
        this.backendService.getCategories().subscribe((categoryList: Array<Category>) => this.categoryList = categoryList);
    }

    deleteCategory(category: Category) {
    }

    editCategory(category: Category) {
    }
}