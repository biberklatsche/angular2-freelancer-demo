import {Component} from '@angular/core';
import {NavbarService, Tabs} from './navbar.service';


@Component({
    selector: 'navbar',
    moduleId: module.id,
    templateUrl: 'navbar.component.html',
    styleUrls: ['navbar.component.css']
})
export class NavbarComponent {
    
    tabs = Tabs;
    navbarService:NavbarService;

    constructor(navbarService: NavbarService){
        this.navbarService = navbarService;
    }

    getCurrentTab(){
        this.navbarService.getCurrentTab();
    }

    setCurrentTab(tab:Tabs){
        this.navbarService.setCurrentTab(tab);
    }

    isActive(tab: Tabs){
        return this.navbarService.getCurrentTab() === tab;
    }
    
}