import {Injectable} from '@angular/core';

@Injectable()
export class NavbarService {
    currentTab = Tabs.Freelancer;

    getCurrentTab(){
        return this.currentTab;
    }

    setCurrentTab(tab: Tabs){
        this.currentTab = tab;
    }
}

export enum Tabs{
    Freelancer, Category
}
