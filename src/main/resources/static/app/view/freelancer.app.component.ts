import {Component} from '@angular/core';
import {BackendService} from '../backend/backend.service';
import {CategoryListComponent} from './category/categoryList.component';
import {FreelancerBrowserComponent} from './freelancer/freelancerBrowser.component';
import {NavbarComponent} from './navbar/navbar.component';
import {NavbarService, Tabs} from './navbar/navbar.service';

@Component({
    selector: 'freelancer-app',
    moduleId: module.id,
    templateUrl: 'freelancer.app.component.html',
    styleUrls: ['freelancer.app.component.css'],
    directives: [CategoryListComponent, FreelancerBrowserComponent, NavbarComponent]
})
export class FreelancerAppComponent {
    tabs = Tabs;
    navbarService : NavbarService;

    constructor(navbarService: NavbarService){
        this.navbarService = navbarService;
    }

    isActive(tab: Tabs){
        return this.navbarService.getCurrentTab() === tab;
    }
}