import {Component, Input, ViewEncapsulation} from '@angular/core';
import {Freelancer} from '../../model/freelancer';
import {FreelancerListComponent} from './freelancerList.component';
import {FreelancerDetailsComponent} from './freelancerDetails.component';

@Component({
  selector: 'freelancer-browser',
  template: `
  <div class="row">
    <freelancer-list [(selected)]="selectedFreelancer" [freelancerList]="freelancerList" [(mode)]="mode" class="col-xs-4"></freelancer-list>
    <freelancer-details [freelancer]="selectedFreelancer" [(freelancerList)]="freelancerList" [(mode)]="mode" class="col-xs-8"></freelancer-details>
  </div>
  `,
  directives: [FreelancerListComponent, FreelancerDetailsComponent],
})
export class FreelancerBrowserComponent {
  selectedFreelancer: Freelancer;
  freelancerList: Array<Freelancer>;
}

export enum Mode{
    Edit, New, View
}