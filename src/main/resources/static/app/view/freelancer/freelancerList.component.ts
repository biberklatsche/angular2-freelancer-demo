import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Freelancer} from '../../model/freelancer';
import {BackendService} from '../../backend/backend.service';
import {Mode} from './freelancerBrowser.component';
import {SearchPipe} from './search.pipe';

@Component({
    selector: 'freelancer-list',
    moduleId: module.id,
    templateUrl: 'freelancerList.component.html',
    styleUrls: ['freelancerList.component.css'],
    pipes: [SearchPipe]
})
export class FreelancerListComponent {
    @Input() freelancerList: Array<Freelancer>;
    @Input() selected: Freelancer;
    @Input() mode: Mode;
    term : String = '';
    @Output() selectedChange: EventEmitter<Freelancer> = new EventEmitter();
    @Output() modeChange: EventEmitter<Boolean> = new EventEmitter();
    private backendService: BackendService;

    constructor(backendService: BackendService) {
        this.backendService = backendService;
        this.backendService.getFreelancers().subscribe((freelancerList: Array<Freelancer>) => this.freelancerList = freelancerList, error => this.handleError(error));
    }

    newFreelancer() {
        this.selectedChange.next(Freelancer.new());
        this.modeChange.next(Mode.New);
    }

    selectFreelancer(freelancer : Freelancer){
        this.selectedChange.next(freelancer)
        this.modeChange.next(Mode.View);
    }

    private handleError(error){
        console.log(error);
    }
}