import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Freelancer} from '../../model/freelancer';
import {BackendService} from '../../backend/backend.service';
import {Mode} from './freelancerBrowser.component';

@Component({
    selector: 'freelancer-details',
    moduleId: module.id,
    templateUrl: 'freelancerDetails.component.html',
    styleUrls: ['freelancerDetails.component.css']
})
export class FreelancerDetailsComponent {
    @Input() freelancer: Freelancer;
    @Input() mode;
    @Input() freelancerList: Array<Freelancer>;
    @Output() freelancerListChange: EventEmitter<Array<Freelancer>> = new EventEmitter();
    @Output() freelancerChange: EventEmitter<Freelancer> = new EventEmitter();
    @Output() modeChange: EventEmitter<Boolean> = new EventEmitter();
    private originalFreelancer: Freelancer;
    private backendService: BackendService;

    constructor(backendService: BackendService) {
        this.backendService = backendService;
        this.backendService.getFreelancers().subscribe((freelancerList: Array<Freelancer>) => this.freelancerList = freelancerList, error => this.handleError(error));
    }

    edit() {
        this.backendService.getFreelancer(this.freelancer.id).subscribe((freelancer) => {
             console.log(freelancer);
        }, error => this.handleError(error));

        this.originalFreelancer = this.freelancer.clone();
        this.modeChange.next(Mode.Edit);
    }

    delete() {
        this.backendService.deleteFreelancer(this.freelancer.id).subscribe(() => {
            this.deleteFreelancerFromInternalList(this.freelancer.id);
            this.freelancer = null;
            this.freelancerListChange.next(this.freelancerList);
        }, error => this.handleError(error));
        this.modeChange.next(Mode.View);
    }

    save() {
        if (this.mode === Mode.New) {
            this.backendService.saveFreelancer(this.freelancer).subscribe((freelancer) => {
                this.freelancer.copyValues(freelancer);
                this.freelancerList.unshift(this.freelancer);
                this.freelancerListChange.next(this.freelancerList);
                this.modeChange.next(Mode.View);
            }, error => this.handleError(error));
        }
        else{
            this.backendService.saveFreelancer(this.freelancer).subscribe(() => {
                this.modeChange.next(Mode.View);
            }, error => {
                this.cancel();
                this.handleError(error)
            });
        }
    }

    cancel() {
        if (this.mode === Mode.New) {
            this.freelancer = null;
        }
        else {
            this.freelancer.copyValues(this.originalFreelancer);
        }
        this.modeChange.next(this.mode = Mode.View);
    }

    isEditable() {
        return this.mode === Mode.Edit || this.mode === Mode.New;
    }

    private handleError(error) {
        console.log(error);
    }

    private deleteFreelancerFromInternalList(id: number) {
        var indexOfItem = 0;
        for (var freelancer of this.freelancerList) {
            if (freelancer.id === id) {
                break;
            }
            indexOfItem++;
        }
        this.freelancerList.splice(indexOfItem, 1);
        this.freelancerChange.emit(null);
    }
}