import {Pipe} from '@angular/core';

@Pipe({
    name: 'search'
})
export class SearchPipe{
    transform(freelancerList, [term]){
        if(freelancerList != null && term != null){
            return freelancerList.filter(
                (freelancer) => freelancer.lastname.toLowerCase().startsWith(term) 
                || freelancer.firstname.toLowerCase().startsWith(term)
                );
        }
        return freelancerList;
    }
}