import {bootstrap} from '@angular/platform-browser-dynamic';
import {FreelancerAppComponent} from './view/freelancer.app.component';
import {BackendService} from './backend/backend.service';
import {NavbarService} from './view/navbar/navbar.service';
import { HTTP_PROVIDERS } from '@angular/http';

bootstrap(FreelancerAppComponent, [BackendService, NavbarService, HTTP_PROVIDERS]);