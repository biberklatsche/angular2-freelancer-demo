﻿# Schritte um das Ding zum Laufen zu bringen

* NodeJS und npm installieren (https://nodejs.org/en/download/) - einfach den Installer ausführen, falls noch nicht installiert.
* Konsole öffnen und ins Verzeichnis 'src/main/java/resources/static' wechseln
* Ausführen von 'npm install'
* Jetzt kannst du im Eclipse/Intellij/Netbeans einfach die Anwendung FreelancerController.java starten
* Seite ist dann unter 'http://localhost:8080' erreichbar