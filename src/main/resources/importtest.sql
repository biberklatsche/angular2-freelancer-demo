INSERT INTO category (id, name) VALUES (1, 'Experte');
INSERT INTO category (id, name) VALUES (3, 'Super Experte');
INSERT INTO category (id, name) VALUES (4, 'Mega Experte');
INSERT INTO category (id, name) VALUES (2, 'Super Mega Experte');

INSERT INTO freelancer (id, category_id, firstname, lastname, price_per_day) VALUES (2, 1, 'Beate', 'Schneider', 10000);
INSERT INTO freelancer (id, category_id, firstname, lastname, price_per_day) VALUES (3, 1, 'Rene', 'Braun', 10000);
INSERT INTO freelancer (id, category_id, firstname, lastname, price_per_day) VALUES (4, 1, 'Christian', 'Lingner', 10000);

INSERT INTO freelancer (id, category_id, firstname, lastname, price_per_day) VALUES (1, 1, 'Dieter', 'Meier', 10000);
INSERT INTO freelancer (id, category_id, firstname, lastname, price_per_day) VALUES (7, 1, 'Ralf', 'Müller', 15000);

INSERT INTO freelancer (id, category_id, firstname, lastname, price_per_day) VALUES (8, 1, 'Rolf', 'Genscher', 14000);
INSERT INTO freelancer (id, category_id, firstname, lastname, price_per_day) VALUES (9, 1, 'Horst', 'Einstein', 13000);

INSERT INTO freelancer (id, category_id, firstname, lastname, price_per_day) VALUES (5, 1, 'Heinz', 'Rasen', 12000);
INSERT INTO freelancer (id, category_id, firstname, lastname, price_per_day) VALUES (6, 1, 'Hans', 'Sprenger', 11000);
